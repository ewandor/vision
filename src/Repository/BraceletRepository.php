<?php

namespace App\Repository;

use App\Entity\Bracelet;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Bracelet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bracelet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bracelet[]    findAll()
 * @method Bracelet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BraceletRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bracelet::class);
        $this->fields = []; //with title, list fields we can search in
    }
}
