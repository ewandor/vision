<?php

namespace App\Repository;

use App\Entity\Infringement;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Infringement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Infringement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Infringement[]    findAll()
 * @method Infringement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfringementRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Infringement::class);
        $this->fields = ['content']; //with title, list fields we can search in
    }
}
