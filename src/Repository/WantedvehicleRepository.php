<?php

namespace App\Repository;

use App\Entity\Wantedvehicle;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Wantedvehicle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wantedvehicle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wantedvehicle[]    findAll()
 * @method Wantedvehicle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WantedvehicleRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wantedvehicle::class);
        $this->fields = ['type', 'numberplate','model', 'content', 'color']; //with title, list fields we can search in
    }
}
