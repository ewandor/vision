<?php

namespace App\Repository;

use App\Entity\Gang;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Gang|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gang|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gang[]    findAll()
 * @method Gang[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GangRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gang::class);
        $this->fields = ['content']; //with title, list fields we can search in
    }
}
