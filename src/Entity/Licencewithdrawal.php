<?php

namespace App\Entity;

use App\Entity\User;
use DateTimeImmutable;
use App\Entity\Document;
use App\Entity\Directory;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\LicencewithdrawalRepository;

/**
 * @ORM\Entity(repositoryClass=LicencewithdrawalRepository::class)
 */
class Licencewithdrawal extends Document
{
    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $type;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Versioned
     */
    private $until;

    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="licencewithdrawals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directory;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->setNeedLegalAccess(true);
        $this->setUntil(new DateTimeImmutable());
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUntil(): ?\DateTimeImmutable
    {
        return $this->until;
    }

    public function setUntil(\DateTimeImmutable $until): self
    {
        $this->until = $until;

        return $this;
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): self
    {
        $this->directory = $directory;

        return $this;
    }
}
