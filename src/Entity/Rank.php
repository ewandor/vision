<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\RankRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=RankRepository::class)
 * @Gedmo\Loggable
 */
class Rank
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="ranks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $mainGroup;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $shortname;

    /**
     * @ORM\Column(type="integer")
     * @Gedmo\Versioned
     */
    private $power;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Gedmo\Versioned
     */
    private $permissions = [];

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="mainRank")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMainGroup(): ?Group
    {
        return $this->mainGroup;
    }

    public function setMainGroup(?Group $MainGroup): self
    {
        $this->mainGroup = $MainGroup;

        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }

    public function getPower(): ?int
    {
        return $this->power;
    }

    public function setPower(int $power): self
    {
        $this->power = $power;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getPermissions(): ?array
    {
        $permissions = $this->permissions;
        // guarantee every user can access directories
        $permissions[] = 'directory_view';

        return array_unique($permissions);
    }

    public function setPermissions(?array $permissions): self
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setMainRank($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getMainRank() === $this) {
                $user->setMainRank(null);
            }
        }

        return $this;
    }
}
