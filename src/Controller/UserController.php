<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\SearchBarType;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

#[Route('/user', name: 'user_')]
class UserController extends AbstractController
{
    #[Route('/', name: 'list')]
    public function list(PaginatorInterface $paginator, Request $request, UserRepository $UserRepository): Response
    {
        $searchForm = $this->createForm(SearchBarType::class);
        $searchForm->handleRequest($request);

        $pagination = $paginator->paginate(
            $UserRepository->getAll()
                ->search(
                    (
                                $searchForm->isSubmitted()
                                && $searchForm->isValid()
                                && $searchForm->getData()['subject'] !== null
                            ) ? $searchForm->getData()['subject'] : null
                )
                ->onlyValid()
                ->getResult(),
            $request->query->getInt('page', 1)
        );

        return $this->render('user/list.html.twig', [
            'controller_name' => 'UserController',
            'pagination' => $pagination,
            'searchForm' => $searchForm->createView()
        ]);
    }

    #[Route('/view/{id}', name: 'view')]
    public function view(User $User): Response
    {
        return $this->render('user/view.html.twig', [
            'controller_name' => 'UserController',
            'user' => $User
        ]);
    }

    #[Route('/view/{id}/sanctions', name: 'view_sanctions')]
    public function viewSanctions(PaginatorInterface $paginator, User $User, Request $request): Response
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $group = $user->getMainGroup();

        if (!$this->IsGranted('sanction', $group)) {
            throw new AccessDeniedHttpException("granted_not_allowed_to_view_sanctions");
        }
        $pagination = $paginator->paginate(
            $User->getSanctions(),
            $request->query->getInt('page', 1)
        );
        return $this->render('user/sanctions.html.twig', [
            'controller_name' => 'UserController',
            'user' => $User,
            'pagination' => $pagination
        ]);
    }

    #[Route('/view/{id}/documents', name: 'view_documents')]
    public function viewDocuments(PaginatorInterface $paginator, User $User, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $User->getDocuments(),
            $request->query->getInt('page', 1)
        );
        return $this->render('user/documents.html.twig', [
            'controller_name' => 'UserController',
            'user' => $User,
            'pagination' => $pagination
        ]);
    }
}
