<?php

namespace App\Form;

use App\Entity\User;
use App\Form\Type\GroupType;
use App\Form\Type\LastnameType;
use App\Form\Type\FirstnameType;
use App\Form\Type\RepeatedPasswordType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'form_placeholder_email'],
            ])
            ->add('firstName', FirstnameType::class)
            ->add('lastName', LastnameType::class)
            ->add('plainPassword', RepeatedPasswordType::class)
            ->add('mainGroup', GroupType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'form_button_register',
                'attr' => ['class' => 'btn-primary'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
