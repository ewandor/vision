<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class DateTimeVisionType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {

        $resolver->setDefaults([
            'view_timezone' => array_key_exists('TZ', $_ENV) ? $_ENV['TZ'] : false
        ]);
    }

    public function getParent(): string
    {
        return DateTimeType::class;
    }
}
