<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class WeightType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => ['placeholder' => 'form_placeholder_weight'],
            'help' => 'form_help_weight',
            'label' => false,
            'required' => false
        ]);
    }

    public function getParent(): string
    {
        return NumberType::class;
    }
}
