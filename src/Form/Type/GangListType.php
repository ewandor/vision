<?php

namespace App\Form\Type;

use App\Entity\Gang;
use App\Repository\GangRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GangListType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {

        $resolver->setDefaults([
            'class' => Gang::class,
            'query_builder' => function (GangRepository $GangRepository) {
                return $GangRepository->createQueryBuilder('g')
                    ->orderBy('g.title', 'ASC');
            },
            'choice_label' => 'title',
            'placeholder' => 'form_placeholder_gang',
            'help' => 'form_help_gang',
            'label' => 'form_label_gang',
            'required' => false
        ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
