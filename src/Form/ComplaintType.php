<?php

namespace App\Form;

use App\Entity\Complaint;
use App\Form\DocumentType;
use App\Form\Type\ContentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ComplaintType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('versus', null, ['label' => 'form_label_versus', 'help' => 'form_help_versus'])
            ->add('content', ContentType::class)
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'form_choice_complaint_status_inprogress' => 'form_choice_complaint_status_inprogress',
                    'form_choice_complaint_status_waiting' => 'form_choice_complaint_status_waiting',
                    'form_choice_complaint_status_ended' => 'form_choice_complaint_status_ended',
                    'form_choice_complaint_status_discontinued' => 'form_choice_complaint_status_discontinued',
                ],
                'label' => 'form_label_complaint_status',
                'priority' => -800
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Complaint::class,
        ]);
    }
}
