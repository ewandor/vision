<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220120204036 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE folder_directory (folder_id INT NOT NULL, directory_id INT NOT NULL, INDEX IDX_66EFA1F162CB942 (folder_id), INDEX IDX_66EFA1F2C94069F (directory_id), PRIMARY KEY(folder_id, directory_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE folder_document (folder_id INT NOT NULL, document_id INT NOT NULL, INDEX IDX_11DC299C162CB942 (folder_id), INDEX IDX_11DC299CC33F7837 (document_id), PRIMARY KEY(folder_id, document_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE folder_directory ADD CONSTRAINT FK_66EFA1F162CB942 FOREIGN KEY (folder_id) REFERENCES folder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE folder_directory ADD CONSTRAINT FK_66EFA1F2C94069F FOREIGN KEY (directory_id) REFERENCES directory (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE folder_document ADD CONSTRAINT FK_11DC299C162CB942 FOREIGN KEY (folder_id) REFERENCES folder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE folder_document ADD CONSTRAINT FK_11DC299CC33F7837 FOREIGN KEY (document_id) REFERENCES document (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE folder_directory');
        $this->addSql('DROP TABLE folder_document');
    }
}
