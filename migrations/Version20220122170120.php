<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220122170120 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sub_group (id INT AUTO_INCREMENT NOT NULL, main_group_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_8184207C2776C832 (main_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_sub_group (user_id INT NOT NULL, sub_group_id INT NOT NULL, INDEX IDX_8164AD98A76ED395 (user_id), INDEX IDX_8164AD9844FB371E (sub_group_id), PRIMARY KEY(user_id, sub_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sub_group ADD CONSTRAINT FK_8184207C2776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE user_sub_group ADD CONSTRAINT FK_8164AD98A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_sub_group ADD CONSTRAINT FK_8164AD9844FB371E FOREIGN KEY (sub_group_id) REFERENCES sub_group (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_sub_group DROP FOREIGN KEY FK_8164AD9844FB371E');
        $this->addSql('DROP TABLE sub_group');
        $this->addSql('DROP TABLE user_sub_group');
    }
}
