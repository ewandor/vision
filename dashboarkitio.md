# Vision design by https://github.com/codedthemes/dashboardkit-free-bootstrap-admin-template

To edit the style of the ui, we clone the repository and edit thoses settings :

- in src/assets/scss/style.scss
13] @import "settings/dark/custom-variables";
14] @import "settings/dark/theme-variables";

- in src/assets/scss/themes/layouts/_pc-header.scss
62] color: $dark;
63] background: $primary;

- gulpfile.js
47] "node_modules/bootstrap/dist/js/bootstrap.bundle.min.js",

last cloning of the repository: 30/11/2021