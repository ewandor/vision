  
# Vision

[![pipeline status](https://gitlab.com/gamexperience/vision/badges/main/pipeline.svg)](https://gitlab.com/gamexperience/vision/-/commits/main)

This is a web panel to allow roleplay server to have an common interface for all publics services  

## Requirements

- Php ^8.0
- Php extensions : bz2, curl, fileinfo, gd, intl, mbstring, mysqli, openssl, pdo_mysql, xsl
- Git (easier way to install and update)
- Composer (<https://getcomposer.org/download/>)
- An SQL server
- A mail server (or a smtp to send mail)
- Donuts (because cops will use this panel)  

## Installation

1. Clone the repo with the tool of your choice

2. Enter the folder created

3. Copy the "dotEnvDemo" file to ".env", and set all required informations

4. Install all dependencies with composer

    ```bash
    composer install --no-scripts
    ```

5. Setup the database with symfony commands

    > info: If you use the symfony cli, you can replace "php bin/console" by
    > "symfony console"

    ```bash
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
    ```

    > info: if you start your database with docker-compose, the database is
    > already created, so ignore the "doctrine:database:create" command

6. Add a new group to Vision

    ```bash
    php bin/console vision:addgroup
    ```

7. Add a new rank to this new group

    ```bash
    php bin/console vision:addrank
    ```
  
8. Add a new user to vision, and choose group and rank

    ```bash
    php bin/console vision:adduser
    ```

    info : the user is created with a random password, so at first time, you need to reset your password before login
    info : If this is your admin account; dont forget to answer yes while asking if user is admin

9. Start vision the way you want ! 

    with symfony cli:

    ```bash
    symfony serve
    ```

    or with php dev server :

    ```bash
    php -S localhost:8000 -t public
    ```

    and navigate to localhost:8000 with your web browser

## Update  

1. Enter the folder containing Vision

2. update by pulling the repo

    ```bash
    git pull
    ```

3. update the database with symfony commands  

    ```bash
    php bin/console doctrine:migrations:migrate
    ```

4. clear cache

    ```bash
    php bin/console cache:clear
    ```

## Login with json token


you need to encode a json string like this :
```php
openssl_encrypt('{"email":"test@test.fr"}', 'aes-256-cbc', 'LOGIN_JSON_AES_KEY', 0)
```
and then use the token with url :

/login/json?token=YOUR_ENCRYPTED_TOKEN_HERE

If the email exist on user list, the user is connected, if not, and error page is displayed

You need to setup the decryption key by env : LOGIN_JSON_AES_KEY

## Scalability

Vision is ready to be used in scaled architecture :

- user's sessions are stored in database
- just share the "public\uploads" folder between Visions instances to get uploaded files accross instances

## Suggest an idea

> [Open an Enhancement issue](https://gitlab.com/gamexperience/vision/-/issues/new?issuable_template=Enhancement) to improve something already existing
or > [Open an Suggestion issue](https://gitlab.com/gamexperience/vision/-/issues/new?issuable_template=Suggestion) to suggest a new idea

## Report a bug

You found something wrong on Vision ?

> [Open an a bug issue ! click here](https://gitlab.com/gamexperience/vision/-/issues/new?issuable_template=Bug)

## Contributing  

Contributions are always welcome!  

Start by installing Vision on your computer, then create a .env.local file with  

```env
APP_ENV=dev
```

Then, our way to do it :

1. install docker and docker compose on your computer  

    <https://www.docker.com>

    (example: <https://www.docker.com/products/docker-desktop>)

2. install symfony cli on your computer

    <https://symfony.com/download>  

3. use docker-compose to run a mysql server and MailDev

    ```bash
    docker-compose up -d
    ```

4. Setup database : read #5 on installation part
    you can also do the #6 - #7 to create groups and ranks, or use inclued fixtures :

    ```bash
    symfony console d:f:l
    ```

5. Create your first Admin user : read #8 on installation part

6. start the symfony web server

    ```bash
    symfony serve
    ```

    with docker and symfony serve, symfony app will communicate the best way with docker

## Base idea by  

- [@Xbird_](https://twitter.com/xbird_)
- @Ajanvier

# Thanks
Thanks to all contributors of Vision
We want to thanks too the community of the French Roleplay server [GTRP](https://www.gtrp.co/), [with a special message](gtrp.co.md)

## Sources  

- Symfony and packages : list in composer.json
- Logo font : <https://fontsfree.net/garde-font-download.html>
- Logo Icon : [@Xbird_](https://twitter.com/xbird_) with icon from FontAwesome
- Design : <https://dashboardkit.io/>
- Icons : cryptofont - feather - fontawesome - material
- Wysiwyg editor : TinyMCE
- Lightbox : <https://trvswgnr.github.io/bs5-lightbox/>
